<?php
$name       = $_POST['name'];
$email      = $_POST['real_email'];
$state      = $_POST['state'];
$honeypot	= $_POST['email'];

/* If website is provided, bail. This is a bot */
if (!empty($honeypot)){die('nice try, bot.');}

//$recipient = "enquiries@hoofbeats.com.au";
$recipient = "enquiries@hoofbeats.com.au";
$subject   = "Sneak Peek Registration";

$message  = strip_tags("You've got a new email registration from hoofbeats.com.au! \n\n *** Subscriber details *** \n\n Name: $name \n email: $email \n state/territory: $state \n\n");

$headers   = "From: Hoofbeats.com.au \n";
$headers  .= "Reply-To: $email";

mail($recipient,$subject,$message,$headers);

/* Redirects browser to the thankyou page */
header("Location: http://www.hoofbeats.com.au/thankyou.html");

?>