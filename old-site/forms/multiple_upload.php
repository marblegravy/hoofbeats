<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><!-- InstanceBegin template="/Templates/main.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" --> 
<title>Forrestfield Photographics : Upload your Photos</title>
<!-- InstanceEndEditable --> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
body {
	background-attachment: scroll;
	background-image:   url(images/bg.jpg);
	background-repeat: no-repeat;
	background-position: 0px 0px;
	background-color: #FFFFFF;
}
-->
</style>
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}

function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
//-->
</script>
<!-- InstanceBeginEditable name="head" --> 
<style type="text/css">
<!--
.para {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	color: #000000;
}
-->
</style>
<!-- InstanceEndEditable -->
</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" onLoad="MM_preloadImages('images/b1-over.jpg','images/b2-over.jpg','images/b3-over.jpg','images/b4-over.jpg','images/b5-over.jpg','images/b6-over.jpg')">
<table width="647" border="0" cellpadding="0" cellspacing="0">
  <!--DWLayoutTable-->
  <tr> 
    <td width="16" height="91">&nbsp;</td>
    <td width="372">&nbsp;</td>
    <td width="1">&nbsp;</td>
    <td width="258">&nbsp;</td>
  </tr>
  <tr> 
    <td rowspan="2" valign="top"><img src="images/left.jpg" width="16" height="41"></td>
    <td rowspan="2" valign="top"><a href="index.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('home','','images/b1-over.jpg',1)"><img src="images/b1.jpg" name="home" width="41" height="41" border="0"></a><img src="images/f1.jpg" width="23" height="41"><a href="steps-to-upload.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('upload','','images/b2-over.jpg',1)"><img src="images/b2.jpg" name="upload" width="41" height="41" border="0"></a><img src="images/f2.jpg" width="23" height="41"><a href="gifts.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('gifts','','images/b3-over.jpg',1)"><img src="images/b3.jpg" name="gifts" width="40" height="41" border="0"></a><img src="images/f3.jpg" width="24" height="41"><a href="cameras.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('camaras','','images/b4-over.jpg',1)"><img src="images/b4.jpg" name="camaras" width="41" height="41" border="0"></a><img src="images/f4.jpg" width="24" height="41"><a href="other.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('other','','images/b5-over.jpg',1)"><img src="images/b5.jpg" name="other" width="39" height="41" border="0"></a><img src="images/f5.jpg" width="25" height="41"><a href="community.html" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('notices','','images/b6-over.jpg',1)"><img src="images/b6.jpg" name="notices" width="40" height="41" border="0"></a></td>
    <td height="33"></td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td height="8"></td>
    <!-- InstanceBeginEditable name="pagetitle" -->
    <td rowspan="2" valign="bottom"><div align="right"><img src="images/t-upload.jpg" width="222" height="32"></div></td>
    <!-- InstanceEndEditable --></tr>
  <tr> 
    <td height="28" colspan="2" valign="bottom"><img src="images/menu.jpg" width="388" height="28"></td>
    <td></td>
  </tr>
  <tr> 
    <td height="19">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr> 
    <td height="326"></td>
    <td colspan="3" valign="top"><!-- InstanceBeginEditable name="content" --> 
      <h3><font color="#FFFFFF" face="Verdana, Arial, Helvetica, sans-serif">Upload 
        your photos here</font> </h3>
      <table width="68%" border="0" cellspacing="0" cellpadding="0">
        <tr class="para"> 
          <td width="5%" height="25" valign="top"><font color="#FFFFFF">1.</font></td>
          <td width="95%" valign="top"> <p><font color="#FFFFFF"><span class="para"><font color="#FFFFFF">Search 
              your computer for photos by clicking on the <strong>Browse</strong> 
              button. </font></span></font></p></td>
        </tr>
        <tr class="para"> 
          <td height="29" valign="top"><font color="#FFFFFF">2.</font></td>
          <td valign="top"><font color="#FFFFFF">Use only 1 field per photo.</font></td>
        </tr>
        <tr class="para"> 
          <td height="39" valign="top"><font color="#FFFFFF">3.</font></td>
          <td valign="top"><font color="#FFFFFF"><span class="para"><font color="#FFFFFF">Once 
            you have clicked on the <strong>Upload</strong> button please wait 
            while your photos are transferred to our website.</font></span> </font></td>
        </tr>
        <tr class="para"> 
          <td height="43" valign="top"><font color="#FFFFFF">4.</font></td>
          <td valign="top"><font color="#FFFFFF">When your photos have been transferred 
            you will see a white screen telling you whether your transfer has 
            been successful or not. </font></td>
        </tr>
        <tr class="para"> 
          <td height="43" valign="top"><font color="#FFFFFF">5.</font></td>
          <td valign="top"><font color="#FFFFFF">Click the back button to return 
            to the site.</font></td>
        </tr>
      </table>
      <table width="426" border="0" align="left" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
        <tr>
<form action="multiple_upload_ac.php" method="post" enctype="multipart/form-data" name="form1" id="form1">
<td>
<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
                <tr>
                  <td><strong><font size="2" face="Verdana, Arial, Helvetica, sans-serif">Upload 
                    your files here</font></strong></td>
</tr>
<tr>
<td>
<input name="ufile[]" type="file" id="ufile[]" size="50" /></td>
</tr>
<tr>
<td>
<input name="ufile[]" type="file" id="ufile[]" size="50" /></td>
</tr>
<tr>
<td>
<input name="ufile[]" type="file" id="ufile[]" size="50" /></td>
</tr>
<tr>
<td align="center"><div align="left">
                      <input type="submit" name="Submit" value="Upload" />
                    </div></td>
</tr>
</table>
</td>
</form>
</tr>
</table></p><p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <!-- InstanceEndEditable --></td>
  </tr>
</table>
</body>
<!-- InstanceEnd --></html>
